#!/usr/bin/python3
import discord
import asyncio
import config
from secrets import token

bot = discord.Client()

async def start_up():  # Things that should be done one time once the bot has started
    await bot.wait_until_ready()
    print("Fort has been constructed")

@bot.event
async def on_raw_reaction_add(payload):
    if payload.message_id == 748070664707244093:
        #Next line finds the guild object, then the role object and assigns it the user. This will get cleaned up later
        await payload.member.add_roles(discord.utils.get(discord.utils.get(bot.guilds, id=747660029598892082).roles, id=747669273970737242),reason="Checked reaction in #welcome")
    elif payload.message_id in config.messageIDEmojiDict:  # If the message is one to be checked for as defined in config.py
        guild = bot.get_guild(config.guildID)
        reaction_member = await guild.fetch_member(payload.user_id)
        addRole = discord.utils.get(guild.roles, id=config.messageIDEmojiDict[payload.message_id][payload.emoji.name])  # Past 3 lines get relavant information about the reaction
        await reaction_member.add_roles(addRole, reason="Added reaction to message in #roles", atomic=True)  # Adds the role


@bot.event
async def on_raw_reaction_remove(payload):  # Same as on_raw_reaction_add event, except removes the role
    if payload.message_id in config.messageIDEmojiDict:
        guild = bot.get_guild(config.guildID)
        reaction_member = await guild.fetch_member(payload.user_id)
        removeRole = discord.utils.get(guild.roles, id = config.messageIDEmojiDict[payload.message_id][payload.emoji.name])
        await reaction_member.remove_roles(removeRole, reason="Removed reaction to message in #roles", atomic=True)



		


bot.loop.create_task(start_up())
bot.run(token)
