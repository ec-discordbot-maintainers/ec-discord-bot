roleChannelID = 747671450885947433
guildID = 747660029598892082
# Dictionaries per message that hold information of what role applies to what reaction
firstHalfRoleEmojiDict = {
    "🌉": 747728629223391272,  # Course 1 Civil
    "⚙️": 747728645782765619,  # Course 2 Mechanical
    "💎": 747728661880242296,  # Course 3 Material
    "🏛️": 747728681497264220,  # Course 4 Architecture
    "⚗️": 747728705555529841,  # Course 5 Chemistry
    "🖥️": 747728725256306728,  # Course 6 EECS
    "🌿": 747728744277344286,  # Course 7 Biology
    "⚛️": 747728762128302150,  # Course 8 Physics
    "💭": 747728781199933542,  # Course 9 Brain and Cognitive Studies
    "💥": 747728799239766067,  # Course 10 Chemical Engineering
    "🏙️": 747728817652629576,  # Course 11 Planning/Urban Planning
    "🌎": 747728841560162336,  # Course 12 Earth and Planetary Sciences
}

secondHalfRoleEmjoiDict = {
    "💵": 747728888666390639,  # Course 14 Economics
    "🏬": 747728907331174471,  # Course 15 Management/Business
    "🚀": 747728926696013834,  # Course 16 Aerospace
    "🗣️": 747728944538583120,  # Course 17 Political Science
    "🔢": 747728962767159347,  # Course 18 Mathematics
    "☣️": 747729001912467477,  # Course 20 Biological Engineering
    "🎭": 747729022523408498,  # Course 21 Culture/Media
    "☢️": 747729042593022034,  # Course 22 Nuclear Science
    "🤔": 747729083986870283,  # Course 24 Philosophy/Linguistics
    "🎞️": 747729617028120648,  # CMS Comparative Media Studies
    "🔬": 747729645847314452,  # STS Science, Technology, and Society
    "❓": 750096477002399814, # Undecided
}

pronounRoleEmojiDict = {
    "❤️": 747668666367344657,  # She/Her Pronoun
    "💜": 747668754460049408,  # They/Them Pronoun
    "💙": 747668727163650200,  # He/Him Pronoun
    "💚": 747668911448653837, # It/Its Pronoun
    "💛": 747668779936383047, # Ask Me/Other Pronoun
}

classYearRoleEmojiDict = {
    "0️⃣": 750402834666618910, # 2020 Class Year
    "1️⃣": 747669152071942165, # 2021 Class Year
    "2️⃣": 747669200558096405, # 2022 Class Year
    "3️⃣": 747669228437372968, # 2023 Class Year
    "4️⃣": 747669253238423562, # 2024 Class Year
	"5️⃣": 872284822263975957, # 2025 Class Year
    "6️⃣": 1017310319254704208, # 2026 Class Year
    "🇬": 750487383069163580, # GRA
}

hallRoleEmojiDict = {
    "🥥": 747668014500937748, #1W Hall
    "🍌": 747668114866438224, #2W Hall
    "🍏": 747668149402599525, #3W Hall
    "🍑": 747668189982490644, #4W Hall
    "🍓": 747668257263059066, #5W Hall
    "🍉": 747668289076985867, #1E Hall
    "🍍": 747668321293434890, #2E Hall
    "🍊": 747668346446544927, #3E Hall
    "🥝": 747668443855061042, #4E Hall
    "🍒": 747668485340921957, #5E Hall
}


messageIDEmojiDict = {
    748076575056199753: firstHalfRoleEmojiDict,
    748076861225173042: secondHalfRoleEmjoiDict,
    748077732734435399: pronounRoleEmojiDict,
    750069258205003866: classYearRoleEmojiDict,
    750071322943684679: hallRoleEmojiDict,
}

